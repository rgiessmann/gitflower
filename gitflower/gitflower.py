####################### header version 2019-03-01 ####################
import hashlib
import io
import os

def sha256sum(src, digest_size_in_bytes=64, length=io.DEFAULT_BUFFER_SIZE):
    sha2 = hashlib.new("sha256", digest_size=digest_size_in_bytes)
    with io.open(src, mode="rb") as fd:
        for chunk in iter(lambda: fd.read(length), b''):
            sha2.update(chunk)
    return sha2.hexdigest()

_mypath = os.path.abspath(__file__)
_mysha256sum = sha256sum(_mypath)
_myfilename = os.path.basename(__file__)

import logging
logger = logging.getLogger(__name__)

logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger()
rootLogger.setLevel(logging.INFO)

fileHandler = logging.FileHandler("{}.{}.log".format(_myfilename, sha256sum(_mypath)[:6]), mode="w")
fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(logging.INFO)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.INFO)
rootLogger.addHandler(consoleHandler)


logger.info("Hi, this is: '{}' as '{}'".format(_myfilename, __name__))
logger.info("I am located here:")
logger.info(_mypath)
logger.info("My sha256sum hash is:")
logger.info(_mysha256sum)
######################################################################


import logging
import os
import git
import csv
import argparse
import sys
import pyblake2
import io
import pkg_resources
import importlib.util
import tempfile


class Gitflower:
    def __init__(self):
        logger = logging.getLogger(__name__)

        self.WORKING_DIR            = os.getcwd()

        self.HOOK_WORKFLOWFILE      = ""
        self.HOOK_LOCATION_FILES    = ""
        self.HOOK_LOCATION_GITREPOS = ""
        self.HOOK_LOCATION_WORKFLOW = ""

        self.INTERNAL_NAME_OF_DOCUMENTATION_FILE = "./workflow_file_documentation.csv"

        self.INTERNAL_NAME_OF_WORKFLOWER_FILE = "workflow.csv"

        self._force = False


    def parse_args(self, argv=None):
        logger = logging.getLogger(__name__)

        if argv is None:
            argv = sys.argv[1:]

        ## prepare ArgumentParser
        parser = argparse.ArgumentParser(description="Assembles a bouquet of flowers \
        from a list of git repositories. \
        %(prog)s will fetch individual files from a specific git repository with \
        a specific commit ID, and will assembly them into a local folder structure \
        according to your whishes.", epilog="")

        parser.add_argument("workflower_file", metavar="<workflower_file>", help="a local file containing the \
        description of your workflow; can be fetched from a git repository as well, \
        if in format <git_repo>,<git_file>,<git_commit>")

        parser.add_argument("-s", "--storage", metavar="FILES_DIR", default="./files/", \
        help="folder into which the files fetched as described in your workflower_file \
        will be placed into (default: ./files/)")

        parser.add_argument("-g", "--git", metavar="GIT_DIR", default="./git/", \
        help="folder into which the git repos which have to be fetched as described in your workflower_file \
        will be placed into (default: ./git/)")

        parser.add_argument("-w", "--workflow", metavar="DOCUMENTATION_DIR", default="./documentation/", \
        help="folder into which the workflow documentation will be placed (default: ./documentation/)")

        parser.add_argument("-n", "--no-run", action='store_true', \
        help="overrides default behavior to execute the last line. If this flag is active, the \
        last line file is fetched, but not executed.")

        parser.add_argument("-d", "--debug", action='store_true', \
        help="This flag activates verbose logging behavior.")

        parser.add_argument("-v", "--verbose", action='store_true', \
        help="This flag activates verbose logging behavior.")

        parser.add_argument("-f", "--force", action="store_true", help='\
        If the indicated _DIR folders are not empty, no files will be placed in there. \
        With this option, however, "dirty" directories are purged automatically. \
        This applies only if the indicated directories are below the current working directory. \
        Directories above the current working directory will not be purged for security reasons.')

        ## processes args from ArgumentParser
        args = parser.parse_args()

        if args.debug == True or args.verbose == True:
            logger.setLevel(logging.DEBUG)
            ## additionally, set all handlers to DEBUG, too
            for h in logger.root.handlers:
                h.setLevel(logging.DEBUG)


        self.HOOK_WORKFLOWFILE      = os.path.abspath(os.path.join(self.WORKING_DIR, args.workflower_file))
        self.HOOK_LOCATION_FILES    = os.path.abspath(os.path.join(self.WORKING_DIR, args.storage))
        self.HOOK_LOCATION_GITREPOS = os.path.abspath(os.path.join(self.WORKING_DIR, args.git))
        self.HOOK_LOCATION_WORKFLOW = os.path.abspath(os.path.join(self.WORKING_DIR, args.workflow))

        self._force = args.force
        self._no_run = args.no_run




    def b2sum(self, src, digest_size_in_bytes=64, length=io.DEFAULT_BUFFER_SIZE):
        """Calculates a BLAKE2 hash sum of a file, given as file name.
        Returns the b2sum.
        """
        b2 = pyblake2.blake2b(digest_size=digest_size_in_bytes)
        with io.open(src, mode="rb") as fd:
            for chunk in iter(lambda: fd.read(length), b''):
                b2.update(chunk)
        return b2.hexdigest()

    def sha256sum(self, src, digest_size_in_bytes=64, length=io.DEFAULT_BUFFER_SIZE):
        sha2 = hashlib.new("sha256", digest_size=digest_size_in_bytes)
        with io.open(src, mode="rb") as fd:
            for chunk in iter(lambda: fd.read(length), b''):
                sha2.update(chunk)
        return sha2.hexdigest()


    def prepare_clean_directory(self, path):
        logger.info("Preparing clean directory: {}".format(path))
        if os.path.exists(path):
            logger.debug("...exists...")
            if self.directory_empty(path):
                logger.debug("...is empty.")
                pass
            else:
                logger.debug("...is not empty...")
                if self._force:
                    if self.directory_below_WORKING_DIR(path):
                        self.rm_fr(path)
                        self.mkdir_p(path)
                        logger.debug("... successfully purged.")
                    else:
                        exception_string = """
                        The directory {path} is not below the current working directory.
                        Directories above the current working directory will not be
                        purged for security reasons.
                        Please specify your paths accordingly."""
                        exception_string = exception_string.format( **locals() )
                        raise Exception(exception_string)
                else:
                    exception_string = """
                    The directory {path} is not empty. No files will
                    be placed in a dirty directory.
                    Specify the flag -f, --force to purge dirty directories automatically."""
                    exception_string = exception_string.format( **locals() )
                    raise Exception(exception_string)
        else:
            logger.debug("did not exist yet...")
            self.mkdir_p(path)
            logger.debug("... successfully created.")


    def directory_empty(self, path):
        if os.listdir(path) == []:
            return True
        else:
            return False

    def directory_below_WORKING_DIR(self, path):
        logger.debug("directory_below_WORKING_DIR?")
        longest_common_path = os.path.commonprefix([self.WORKING_DIR, path])
        if self.WORKING_DIR in longest_common_path:
            logger.debug(True)
            return True
        else:
            logger.debug(False)
            return False

    def rm_fr(self, path):
        exec_string = 'rm -fr "' + path + '"'
        logger.debug(exec_string)
        exit_status = os.system(exec_string)
        logger.debug(exit_status)
        return exit_status

    def mkdir_p(self, path):
        #print path
        if path == "":
           path = "./"
        path_parts = path.split("/")
        for i, p in enumerate(path_parts):
            i = i+1
            current_dir_to_create = "/".join(path_parts[:i])
            #logger.info(current_dir_to_create)
            if current_dir_to_create == "":
                continue
            if os.path.exists(current_dir_to_create):
                continue
            logger.debug("os.mkdir({current_dir_to_create})".format(**locals()))
            exit_status = os.mkdir(current_dir_to_create)
            logger.debug(exit_status)
        return

    def whether_get_workflow_from_git(self):
        if os.path.exists(self.HOOK_WORKFLOWFILE):
            logger.info("Fetching workflow from local...")
            return False
        else:
            logger.info("Fetching workflow from git...")
            return True

    def init_directories(self):
        _process_list = [self.HOOK_LOCATION_GITREPOS, self.HOOK_LOCATION_FILES, self.HOOK_LOCATION_WORKFLOW]
        for _entry_to_process in _process_list:
            self.prepare_clean_directory(_entry_to_process)
        return

    def where_to_get_workflow(self):
        path = self.HOOK_WORKFLOWFILE
        return path

    def set_internal_name_of_workflower_file(self, name):
        self.INTERNAL_NAME_OF_WORKFLOWER_FILE = name
        return

    def get_internal_name_of_workflower_file(self):
        return self.INTERNAL_NAME_OF_WORKFLOWER_FILE


    def where_to_store_workflow(self):
        path = os.path.join(self.WORKING_DIR , self.HOOK_LOCATION_WORKFLOW , self.get_internal_name_of_workflower_file() )
        return path

    def where_to_find_this_repo(self, reponame):
        path = os.path.join(self.HOOK_LOCATION_GITREPOS, reponame)
        return path

    def path_to_workflow_repo(self):
        path = os.path.join(self.HOOK_LOCATION_GITREPOS, "workflow_git")
        return path

    def extract_a_filename_from_a_path(self, path_string):
        cutoff_left = path_string.rfind("/") +1
        filename = path_string[cutoff_left:]
        return filename


    def extract_directory_from_a_string_with_wildcard(self, original_string_with_wildcard):
        logger.info("I am replacing an wildcard in storage_filename with the filename used in the repo. This is intended behavior.")
        given_directory_part = os.path.dirname(original_string_with_wildcard)
        if given_directory_part == "":
            given_directory_part = "."
        ret = given_directory_part + "/"
        return ret

    def __get_a_single_file_from_a_remote_git_repo(self):
        ## TODO

        exec_string = 'git archive --format=tar --remote=ssh://git@gitlab.tu-berlin.de/hts_modelling/data_of_master_thesis_NK_Niels-Krausch.git heads/master -- "SOP/NK_02_Data handling.pdf" > tmp.tar && tar -xf tmp.tar && cat "SOP/NK_02_Data handling.pdf" '

        return

    def execute(self):
        logger = logging.getLogger(__name__)

        # prepare directories
        self.init_directories()

        ## get workflow
        if self.whether_get_workflow_from_git() == True:
            _a = self.HOOK_WORKFLOWFILE.split(",")
            _repo = _a[0]
            _file = _a[1]
            _commit = _a[2]

            workflow_repo = git.Repo.clone_from( _repo, path_to_workflow_repo() )

            exec_dict = {}
            exec_dict["path_to_workflow_repo"] = self.path_to_workflow_repo()
            exec_dict["git_commit"] = _commit
            exec_dict["git_file"] = _file
            self.set_internal_name_of_workflower_file(_file)
            exec_dict["where_to_store_workflow"] = self.where_to_store_workflow()

            exec_string = 'cd "{path_to_workflow_repo}" && git show {git_commit}:"{git_file}" > "{where_to_store_workflow}"'.format( **exec_dict )

            os.system(exec_string)
        else:
            _from = self.where_to_get_workflow()
            logger.debug("_from?")
            logger.debug(_from)
            logger.debug(os.path.basename(_from))
            self.set_internal_name_of_workflower_file(os.path.basename(_from))
            logger.debug(self.INTERNAL_NAME_OF_WORKFLOWER_FILE)
            _to   = self.where_to_store_workflow()
            logger.debug("_to?")
            logger.debug(_to)
            os.system('cp "{}" "{}"'.format(_from,_to))
        ## workflow is now in:  where_to_store_workflow()


        ## read workflow...
        filehandle_input = open( self.where_to_store_workflow() )
        input_reader = csv.DictReader(filehandle_input)


        ## check whether all expected fields are there, otherwise prepare them
        expected_fields = ["absolute_path_to_file", "b2sum", "sha256sum", "local_file", "git_package", "git_file", "git_commit", "storage_filename"]
        additional_fields = []
        for e in expected_fields:
            if e not in input_reader.fieldnames:
                additional_fields.append(e)

        ## read workflow row by row, dropping empty lines
        workflow_rows = []
        for csv_row in input_reader:
            _empty = True
            for key in csv_row:
                if csv_row[key] != "":
                    _empty = False
            if _empty == True:
                continue
                ## drop line
            else:
                ## supplement csv_row with missing fields
                for a in additional_fields:
                    csv_row.update({a: ""})
                workflow_rows.append(csv_row)

        ## check input for plausibility:
        logger.debug("checking input for plausibility")
        for row in workflow_rows:
            logger.debug("checking row: {}".format(row))

            def assert_git_is_complete():
                fields = ["git_package", "git_file"] ## "git_commit" can be empty
                _complete = True
                _none_existent = True
                for f in fields:
                    if row[f] != "":
                        _none_existent = False
                    else:
                        _complete = False
                assert(_complete or _none_existent)
            def assert_either_git_or_file():
                gitfields = ["git_package", "git_file", "git_commit"]
                filefields = ["local_file"]
                _none_of_git = True
                _none_of_file = True
                for f in gitfields:
                    if row[f] != "":
                        _none_of_git = False
                for f in filefields:
                    if row[f] != "":
                        _none_of_file = False
                assert(_none_of_file or _none_of_git)

            assert_git_is_complete()
            assert_either_git_or_file()


        ## open output
        filehandle_documentation = open(self.INTERNAL_NAME_OF_DOCUMENTATION_FILE, "w")
        docwriter = csv.DictWriter(filehandle_documentation, input_reader.fieldnames+additional_fields)
        docwriter.writeheader()

        ## create a new repo and work off the workflow
        _repo = git.Repo.init()
        for i in workflow_rows:
            logger.info("This is entry:")
            logger.info("----------------------------")
            logger.info(i)
            logger.info("----------------------------")


            def is_git_entry():
                if i["git_package"] != "":
                    return True
                else:
                    return False
            def is_local_file_entry():
                if i["local_file"] != "":
                    return True
                else:
                    return False

            ## if filename is empty, replace it with the one found in the git repo
            if i["storage_filename"] == "" or i["storage_filename"].split("/")[-1] == "*":
                old = i["storage_filename"]

                ## automagic completion of wildcard to original filename
                if i["storage_filename"].split("/")[-1] == "*":
                    i["storage_filename"] = self.extract_directory_from_a_string_with_wildcard(i["storage_filename"])

                ## do automatic completion of optional field storage_filename
                if i["storage_filename"] == "":
                    logger.info("I am replacing an empty storage_filename with the filename used in the repo. This is intended behavior.")
                    i["storage_filename"] = "./"

                if is_git_entry():
                    new_filename = self.extract_a_filename_from_a_path(i["git_file"])
                if is_local_file_entry():
                    new_filename = self.extract_a_filename_from_a_path(i["local_file"])

                i["storage_filename"] += new_filename
                logger.info("'"+old+"' becomes "+i["storage_filename"]+" now.")


            ## creates local directory
            absolute_path_to_copy_to = os.path.abspath(self.HOOK_LOCATION_FILES)
            str_containing_local_directory = i["storage_filename"]
            local_directory = os.path.dirname(str_containing_local_directory)
            directory = os.path.join(absolute_path_to_copy_to, local_directory)
            self.mkdir_p(directory)

            ## sets the absolute path for the file to be fetched
            absolute_path_to_file = os.path.abspath(self.HOOK_LOCATION_FILES)
            i["absolute_path_to_file"] = absolute_path_to_file
            i["absolute_path_to_file"] = i["absolute_path_to_file"] + "/" + i["storage_filename"]


            if is_git_entry():
                logger.debug("Working off a git entry...")
                ## extracting repo name
                ## assuming git_packages in ssh , not https
                str_contains_reponame = i["git_package"]
                cutoff_left  = str_contains_reponame.rfind("/") +1
                cutoff_right = str_contains_reponame.rfind(".git")
                if cutoff_right == -1:
                    cutoff_right = None
                repo = str_contains_reponame[cutoff_left:cutoff_right]
                logger.info("Entering repo: '{}'...".format(repo))

                ## clone
                if os.path.exists( self.where_to_find_this_repo(repo) ):
                    repo_for_this_entry = git.Repo( self.where_to_find_this_repo(repo) )
                else:
                    if os.path.exists( i["git_package"] ):
                        repo_for_this_entry = git.Repo( i["git_package"] )
                        os.system("ln -s '"+i["git_package"]+"' '"+self.where_to_find_this_repo(repo)+"'")
                    else:
                        repo_for_this_entry = _repo.clone_from( i["git_package"], self.where_to_find_this_repo(repo) )

                ## do the automagic correction for git_commit field
                if i["git_commit"] == "":
                    logger.info("I am replacing an empty commit field with 'HEAD'. This is intended behavior.")
                    i["git_commit"] = "HEAD"
                ## replace HEAD with concrete commit SHA
                if i["git_commit"] == "HEAD":
                    head_commit = repo_for_this_entry.commit("HEAD")
                    head_sha = head_commit.hexsha
                    i["git_commit"] = head_sha

                ## place file to correct place
                exec_string = 'cd "'+self.where_to_find_this_repo(repo)+'" && git show {git_commit}:"{git_file}" > "{absolute_path_to_file}"'.format( **i )
                logger.debug("Executing this:")
                logger.debug("+++")
                logger.debug(exec_string)
                logger.debug("+++")
                return_value = os.system(exec_string)

                ## check success
                if return_value != 0:
                    logger.critical("Couldn't find this file! I am aborting here.")
                    sys.exit(2)

            if is_local_file_entry():
                logger.debug("Working off a local file entry...")
                ## place file to correct place
                exec_string = 'cp "{local_file}" "{absolute_path_to_file}"'.format( **i )
                logger.debug("Executing this:")
                logger.debug("+++")
                logger.debug(exec_string)
                logger.debug("+++")
                return_value = os.system(exec_string)

                ## check success
                if return_value != 0:
                    logger.critical("Couldn't find this file! I am aborting here.")
                    sys.exit(2)


            ## verify file integrity

            ## generate checksums
            b2s = self.b2sum(i["absolute_path_to_file"])
            sha256s = self.sha256sum(i["absolute_path_to_file"])

            ## check b2sum if given
            if i["b2sum"] != "":
                if not b2s == i["b2sum"]:
                    exception_string = "File {} does not yield the b2sum hash indicated in the workflow file!".format(i["absolute_path_to_file"])
                    raise Exception(exception_string)
                elif b2s == i["b2sum"]:
                    logger.info("b2sum of file {} matches.".format(i["absolute_path_to_file"]))

            ## check sha256sum if given
            if i["sha256sum"] != "":
                if not sha256s == i["sha256sum"]:
                    exception_string = "File {} does not yield the sha256sum hash indicated in the workflow file!".format(i["absolute_path_to_file"])
                    raise Exception(exception_string)
                elif sha256s == i["sha256sum"]:
                    logger.info("sha256sum of file {} matches.".format(i["absolute_path_to_file"]))

            ## store b2sum to get documented
            i["b2sum"] = b2s
            i["sha256sum"] = sha256s

            ## document downloaded file
            docwriter.writerow(i)

            logger.info("End of entry.")
            logger.info("------------------------------")


        filehandle_documentation.close()
        filehandle_input.close()

        logger.info("------------------------------")
        logger.info("Finished successful.")
        logger.info("------------------------------")


        def get_sorted_file_checksums(type_of_checksum):
            with open(self.INTERNAL_NAME_OF_DOCUMENTATION_FILE) as csvfile:
                checksum_reader = csv.DictReader(csvfile)
                #print(self.INTERNAL_NAME_OF_DOCUMENTATION_FILE)
                list_of_file_checksums = []
                for row in checksum_reader:
                    #print(row.keys())
                    list_of_file_checksums.append(row[type_of_checksum])
            return sorted(list_of_file_checksums)


        def calculate_b2sum_for_all_file_checksums():
            sorted_file_checksums = get_sorted_file_checksums("b2sum")
            string_of_sorted_file_checksums = "".join(sorted_file_checksums)

            tmp_output = tempfile.NamedTemporaryFile()
            tmp_output.write(bytes(string_of_sorted_file_checksums, 'UTF-8'))
            tmp_output.seek(0)
            #os.system('cp "{}" /tmp/tmpb'.format(tmp_output.name))

            b2sum = self.b2sum(tmp_output.name, digest_size_in_bytes=2)

            tmp_output.close()

            return b2sum

        def calculate_sha256sum_for_all_file_checksums():
            sorted_file_checksums = get_sorted_file_checksums("sha256sum")
            string_of_sorted_file_checksums = "".join(sorted_file_checksums)

            tmp_output = tempfile.NamedTemporaryFile()
            tmp_output.write(bytes(string_of_sorted_file_checksums, 'UTF-8'))
            tmp_output.seek(0)
            #os.system('cp "{}" /tmp/tmps'.format(tmp_output.name))

            sha256sum = self.sha256sum(tmp_output.name)

            tmp_output.close()

            return sha256sum


        file_checksum_b = calculate_b2sum_for_all_file_checksums()
        file_checksum_s = calculate_sha256sum_for_all_file_checksums()

        logger.info("------------------------------")
        logger.info("Total b2sum of sorted file checksums is:")
        logger.info(file_checksum_b)
        logger.info("Total sha256sum of sorted file checksums is:")
        logger.info(file_checksum_s)
        logger.info("------------------------------")

        with open("files_checksum.b2sum", "w") as f:
            f.write(file_checksum_b)
        with open("files_checksum.sha256sum", "w") as f:
            f.write(file_checksum_s)




        total_checksum_b = self.b2sum(self.INTERNAL_NAME_OF_DOCUMENTATION_FILE, digest_size_in_bytes=2)
        total_checksum_s = self.sha256sum(self.INTERNAL_NAME_OF_DOCUMENTATION_FILE, digest_size_in_bytes=2)


        logger.info("------------------------------")
        logger.info("Total b2sum of workflow is:")
        logger.info(total_checksum_b)
        logger.info("Total sha256sum of workflow is:")
        logger.info(total_checksum_s)
        logger.info("------------------------------")

        with open("workflow_checksum.b2sum", "w") as f:
            f.write(total_checksum_b)
        with open("workflow_checksum.sha256sum", "w") as f:
            f.write(total_checksum_s)

        if not self._no_run:
            logger.info("++++++++++++++++++++++++++++++")
            logger.info("Starting execution of the last file...")
            logger.info("++++++++++++++++++++++++++++++")

            storage_filename = i["storage_filename"]
            spec = importlib.util.spec_from_file_location(storage_filename, i["absolute_path_to_file"])
            data_toolbox = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(data_toolbox)


        else:
            logger.info("++++++++++++++++++++++++++++++")
            logger.info("Not executing anything.")
            logger.info("++++++++++++++++++++++++++++++")

        logger.info("++++++++++++++++++++++++++++++")
        logger.info("Done.")



def main():
    g = Gitflower()
    g.parse_args()
    g.execute()

if __name__ == "__main__":
    main()
