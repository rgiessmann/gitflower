# What is gitflower?

gitflower allows you to fetch files from different git repositories into one
common file structure.


This enables you to produce single files ("workflow descriptions"), which allow
other users to reproduce your workflow in every detail.

![alt text](doc/images/gitflower_illustration.png "Illustration of the working principle of gitflower")


# A statement of need

When working with research software, we are working with continuously changing
alpha versions and/or changing data sets. Working in heavily fractured IT
environments (e.g. on individual computers which are not administrated by a
central department), we found ourselves in need of an easily applicable tool
to generate reproducible workflows.

Versioning control systems like git are already of great use in increasing
reproducibility and providing unambiguous identification of specific file versions.
Web interfaces to version-controlled repositories, e.g. GitHub, GitLab, but
somewhat related also OSF, allow researchers to control changing data sets
easily.

However, when working with objects (e.g. software, data sets) from different
sources (e.g. different git repositories for software and data, respectively),
we could not find a simple solution to compile specific versions of specific
objects from different repositories into a file structure on local file systems.

Current workflow solutions like Apache Taverna or KNIME are excellent at
organizing workflows of stable software with varying data, but are hard to use
as flexible as needed in the field of continuously evolving research software.

This need is addressed by gitflower. Gitflower will help researchers to share
unambiguous workflow descriptions including experimental research software and
(private) data sets, which are fetched from git repositories.


# Installation instructions

Gitflower is usable with Python 2 and Python 3, and depends on gitpython and pyblake2.

For general ease of use, we recommend using a python distribution like anaconda.


This package is ready to be installed with pip.

Clone the repo and install gitflower with the following commands in your local shell:

```
git clone https://r.giessmann@gitlab.tubit.tu-berlin.de/r.giessmann/gitflower.git
cd gitflower
pip install .
```

Gitflower will be registered as a command-line interface during installation, so
you can access it within your local shell as easy as:

```
gitflower <workflower_file>
```


# Example usage

We provide with this package an examplary workflower file, You can access it by entering in your local shell:

```
gitflower_illustration
```
The authors should include examples of how to use the software (ideally to solve real-world analysis problems).


# API documentation

Reviewers should check that the software API is documented to a suitable level. This decision is left largely to the discretion of the reviewer and their experience of evaluating the software.

Good: All functions/methods are documented including example inputs and outputs
OK: Core API functionality is documented
Bad (not acceptable): API is undocumented


# Tests

Authors are strongly encouraged to include an automated test suite covering the core functionality of their software.

Good: An automated test suite hooked up to an external service such as Travis-CI or similar
OK: Documented manual steps that can be followed to check the expected functionality of the software (e.g. a sample input file to assert behaviour)
Bad (not acceptable): No way for you the reviewer to check whether the software works


# Community guidelines

There should be clear guidelines for third-parties wishing to:

Contribute to the software
Report issues or problems with the software
Seek support
