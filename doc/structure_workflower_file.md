# Structure of Workflower Files

A workflower file is a simple .csv file. 

You find a template for your work here: [./template_workflower.csv](./template_workflower.csv)  

 

It contains the following columns:

  * `git_package`
  * `git_file`	
  * `git_commit`	
  * `storage_filename`




`git_package` is either a https/ssh URI to a git repository online, or a URI to a local git repository (located on your computer).

`git_file` is referencing the file in the repository, starting from ./ for the repository's base URI.

`git_commit` is the commit SHA, consisting of 1) the full SHA hash, 2) an abbreviated SHA hash (with at least 8 characters), or 3) HEAD as referencing to the most current commit SHA.

`storage_filename` indicates the location where the file will be stored in your local file system, below the directory "files" (or whatever you set by the flag -s, --storage).

