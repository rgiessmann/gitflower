from setuptools import setup, find_packages

setup(
    name='gitflower',
    version='0.4.0',
    packages=find_packages(exclude=['test', 'test.*']),
    include_package_data=True,
    platforms='any',
    install_requires=[
        'gitpython',
        'pyblake2'
    ],
    entry_points={
        'console_scripts': [
            'gitflower=gitflower.gitflower:main',
        ],
    }
)
